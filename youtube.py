#!/usr/bin/env python3
# encoding: utf-8

"""
File: youtube.py
Author: Panagiotis S.
Github: https://github.com/PanagiotisS/
Description: Backup your youtube playlists.
"""

from bs4 import BeautifulSoup
from urllib.request import urlopen


def main():
    """
    The main function.
    """
    while True:
        choice = answer(
            "\nWould you like to download one playlist (1)\n"
            "\tor all playlists from a user (2).", "2")
        if choice == "1":
            playlist = answer(
                "\nPlease provide a youtube playlist url.",
                "https://www.youtube.com/watch?v=0pPHavgtRVk&list=PLXiyafFeKBLdvgYUVEoXnkLxxoBZcHFmA")
            find_songs(playlist)
            return
        elif choice == "2":
            user = answer(
                "\nPlease provide a youtube user's playlist url.",
                "https://www.youtube.com/channel/UCfGr8L9BIFCzAiKJwuMa1_w/playlists")
            find_playlists(user)
            return


def answer(question, default):
    """
    Ask a question
    """
    while True:
        print("{:s}\nDefault: {:s}".format(question, default))
        choice = input()
        if choice != '':
            return choice
        else:
            return default


def find_songs(link):
    """
    Input: Playlist link
    Output: The file with the songs name and link
    """
    source = urlopen(link)
    soup = BeautifulSoup(source, "html.parser")

    list_title = soup.title.string.splitlines()[0].replace(" ", "")
    list_title = list_title.replace("-YouTube", "")
    songs = soup.find_all("tr")
    output(list_title, songs)


def output(list_title, songs):
    """
    Writes the output file

    :list_title: the list tile
    :songs: the songs' name
    """
    ytw = 'https://www.youtube.com/watch?v='
    with open(list_title + '.rst', encoding='utf-8', mode='w') as outfile:
        outfile.write(('{1:s}\n{0:s}\n{1:s}\n').format(
            list_title, "="*len(list_title)))
        for song in songs:
            outfile.write('|\n| {:s}\n| {:s}{:s}\n'.format(
                song['data-title'], ytw, song['data-video-id']))
        outfile.write("\n")


def find_playlists(link):
    """
    Input: User's all playlists url
    Output: Playlist url
    """
    ytw = 'https://www.youtube.com'
    source = urlopen(link)
    soup = BeautifulSoup(source, "html.parser")

    owner_title = soup.title.string.splitlines()[0].replace(" ", "")
    elem = soup.find_all(attrs={"aria-describedby": True})

    print("\n{:s}{:s}\n".format(
        'the owner of the playlist is: ', owner_title))

    for plist in elem:
        link = ytw + plist['href']
        find_songs(link)
        print("{:s}\n{:s}\n".format(plist.string, link))


if __name__ == '__main__':
    main()
