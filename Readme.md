# youtubeBackUp
Backup your playlists from youtube.  

## How to
Just run the script which will download the playlist(s) into rst files.  
Then using git you can keep track of changes of rst files and find out which videos got deleted/removed etc.  

You can either download a single playlist or all playlists from a single user.  

## Limitations
- The playlists must be max 100 videos per playlist.  
- Python 3 (beatifulSoup, urllib).  

## Default Playlist/User
At lines [L26](https://github.com/PanagiotisS/youtubeBackUp/blob/master/youtube.py#L26) and [L32](https://github.com/PanagiotisS/youtubeBackUp/blob/master/youtube.py#L32) you may add a default playlist and user respectively.

P.S. User "legocombo" who is now as default choice is just a random user with playlists which I enjoy listening. 
No affiliation or whatsoever.
